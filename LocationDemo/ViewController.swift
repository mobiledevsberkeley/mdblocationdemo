//
//  ViewController.swift
//  LocationDemo
//
//  Created by Virindh Borra on 11/6/15.
//  Copyright © 2015 Virindh Borra. All rights reserved.
//

import UIKit
//Import corelocation later on in the tutorial
import CoreLocation

//Implement the location delegate
class ViewController: UIViewController, CLLocationManagerDelegate {

    //Connected from the story board
    @IBOutlet weak var mainLabel: UILabel!
    
    //Create an empty current location object
    var currentLocation:CLLocation! = nil
    
    //Create the location manager
    let locationManager = CLLocationManager()

    //Method to execute when the label gets "tapped"
    func mainLabelClicked(gestureRecognizer : UIGestureRecognizer)
    {
        //Lets get the location of the user and display it in an alert
        
        
        //Display an alert appropriately
        
        //Create an empty alert view controller
        var alertCont:UIAlertController! = nil
        
        //Check if the current location is empty
        if currentLocation == nil
        {
            //Current location is empty, display an alert that it's empty
            
            //Create the appropriate alert view controller
            alertCont = UIAlertController(title: "No Location", message: "It looks like we don't have your location yet! Try again later.", preferredStyle: .Alert)
            
            //Create the okay action/button
            let okayAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            
            //Add the action
            alertCont.addAction(okayAction)
            
            //Present the alert
            self.presentViewController(alertCont, animated: true, completion: nil)
            
            
        }else
        {
            //Current location exists! Show the alert
            
            //Create a geocoder
            let geocoder = CLGeocoder()
            
            geocoder.reverseGeocodeLocation(currentLocation, completionHandler: { (placemarks, error) -> Void in
                
                //Get city name
                
                //Get the placemark
                let placemark = placemarks![0] as CLPlacemark
                

                //Set the city name
                let cityName = placemark.locality
                
                //Create the message for the user
               let messageToUser = "Hey! You're currently in \(cityName!)."
                
                //Create the appropriate alert view controller
                alertCont = UIAlertController(title: "Current Location", message: messageToUser, preferredStyle: .Alert)
                
                //Create the okay action/button
                let okayAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                
                //Add the action
                alertCont.addAction(okayAction)
                
                //Present the alert
                self.presentViewController(alertCont, animated: true, completion: nil)

            })
            

           

        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Create tap gesture recognizer
        let tapGest:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("mainLabelClicked:"))
        
        //Make it a two finger button
        tapGest.numberOfTouchesRequired = 1
        
        //Add the gesture recognizer to the label
        mainLabel.addGestureRecognizer(tapGest)
        
        //At this point explain the locatoin structure from iOS 8 and how an info.plist object(NSLocationWhenInUseUsageDescription or NSLocationAlwaysUsageDescription) must be set
        //Set the Plist
        //Import CoreLocation module
        //Conform to the location delegate
        
        
        //Set ourselves as the delegate
        locationManager.delegate = self
        
        //Check the current status
        if CLLocationManager.authorizationStatus() == .NotDetermined
        {
            //If the status is not determined, request it for use within the app
            locationManager.requestWhenInUseAuthorization()
        }
        
        
        
        
        
    }
    
    
    //Delegate method that gets called whenever the user changes location authorization status
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        //Authorized for us to use location
        if status == .AuthorizedWhenInUse {
            
            
            //Now we call start updating location
            manager.startUpdatingLocation()
        }
        
    }
    
    //Function that gets called whenever location manager updates locations
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        //Set the current location to the updated locations(if there are multiple, select one)
        currentLocation = locations.last
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

